git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: O comando git init cria um novo repositório do Git ou reinicializa um existente.

git status: O comando git status exibe as condições do diretório de trabalho e da área de staging.

git add arquivo/diretório : O comando git add, adiciona uma alteração no diretório ativo à área de staging. Ele diz ao Git que você quer incluir atualizações a um arquivo específico no próximo commit.
git add --all = git add . : Adiciona todas as alterações feitas no diretório ativo à área de staging.

git commit -m “Primeiro commit”: O comando faz uma captura ("checkpoint") do estado do projeto naquele momento e adiciona uma mensagem (comentário) para que o commit possa ser entendido, ou seja, por exemplo a mensagem serve para descrever oque foi mudado.

-------------------------------------------------------

git log: O comando exibe o registro de todos os commits realizados, suas datas e informações.
git log arquivo: Mostra o registro de commit de um arquivo específico .
git reflog: O comando exibe um commit de forma resumida, ou seja, com poucas informações.

-------------------------------------------------------

git show : Exibe os detalhes do último commit 
git show <commit> : Exibe detalhes do commit específicado

-------------------------------------------------------

git diff : Faz a comparação entre o que está na área de trabalho e commit mais recente.
git diff <commit1> <commit2> : Faz a comparação entre dois commits. Mostra quais foram os arquivos alterados, novos e
removidos. Além disso, mostra também quais foram as linhas alteradas.

-------------------------------------------------------

git reset --hard <commit> Desfaz todas as alterações que aconteceram em um commit específico.


-------------------------------------------------------

git branch: O comando exibe uma lista com todas as branches locais.
git branch -r: O comando faz a listagem de todas as branches remotas
git branch -a: O comando faz a listagem de todas as branches locais e remotas
git branch -d <branch_name>: O comando faz a remoção de um branch local.
git branch -D <branch_name>: Força a remoção de um branch local, caso esta branch
ainda não tenha feito o merge
git branch -m <nome_novo>: O comando Renomeia a branch atual 
git branch -m <nome_antigo> <nome_novo>: Renomeia uma branch específicada pelo nome_antigo para o nome_novo

-------------------------------------------------------

git checkout <branch_name>: Acessa a branch selecionada.
git checkout -b <branch_name>: Cria a branch e é direcionado para ela.

-------------------------------------------------------

git merge <branch_name>: Mescla as mudanças presentes na branch específica na branch corrente.

-------------------------------------------------------

git clone: O Comando realiza a cópia do repositório para uma pasta local.
git pull: O Comando atualiza repositório local com a última versão da origem da branch remota.
git push: O Comando faz o envio das mudanças comitadas localmente para a origem da branch rastreada.

-------------------------------------------------------

git remote -v: O comando faz a listagem dos servidores remotos que o repositório está usando associados com a URL.
git remote add origin <url>: O comando adiciona um enderço remoto para o seu repositório local 
git remote <url> origin: Indica a URL do repositório remoto como repositório origin.

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


